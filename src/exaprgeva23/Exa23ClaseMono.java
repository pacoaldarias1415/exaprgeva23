/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exaprgeva23;

/**
 * Fichero: Exa23ClaseMono.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-feb-2014
 */
public class Exa23ClaseMono extends Exa23ClaseAbstracta {

  public Exa23ClaseMono(String name) {
    super(name);
  }

  // Implement the abstract method
  public void walk() {
    System.out.println("Mono " + getName() + " también anda...");
  }
}
