package exaprgeva23;

public class Exa23ClasePersona extends Exa23ClaseAbstracta {

  public Exa23ClasePersona(String name) {
    super(name); // 6
  }

  public void walk() {
    System.out.println("Humano " + getName() + " anda...");
  }
}
