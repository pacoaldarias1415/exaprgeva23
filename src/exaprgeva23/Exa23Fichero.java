package exaprgeva23;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.StringTokenizer;

public class Exa23Fichero {

  public static void main(String[] args)
          throws IOException {
    String fichero = "fichero1.txt";
    String frases[] = {"hola adios", "como estas", "yo bien"};
    int numpalabras = 0;
    int numlineas = 0;
    String linea;
    FileWriter w = new FileWriter(fichero); //(2)
    for (int i = 0; i < frases.length; i++) {
      w.write(frases[i]); //(3)
      w.write("\r\n"); //(4)
    }
    w.close();
    BufferedReader r =
            new BufferedReader(new FileReader(fichero)); //(5)
    while ((linea = r.readLine()) != null) { //(6)
      StringTokenizer stringTokenizer = new StringTokenizer(linea);
      numpalabras = numpalabras + stringTokenizer.countTokens(); //(7)
      System.out.println("Linea " + numlineas + ": " + linea);
      numlineas++;//(8)
    }
    r.close();
    System.out.println("Total Lineas " + numlineas);
    System.out.println("Total Palabras " + numpalabras);
  } // main
} // class

/* EJECUCION:
 Linea 0: hola adios
 Linea 1: como estas
 Linea 2: yo bien
 Total Lineas 3
 Total Palabras 6
 */
