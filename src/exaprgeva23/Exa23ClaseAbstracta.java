package exaprgeva23;

public abstract class Exa23ClaseAbstracta { // 3

  private String name;

  public Exa23ClaseAbstracta(String name) {
    this.name = name; // 4
  }

  public abstract void walk(); //5

  public String getName() {
    return name;
  }
}
