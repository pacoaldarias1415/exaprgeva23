package exaprgeva23;

public class Exa23ClaseMain {

  public static void main(String[] args) {

    Exa23ClasePersona humano1 = new Exa23ClasePersona("Paco"); //1
    humano1.walk();

    Exa23ClaseAbstracta cosaviviente1 = humano1; //2
    cosaviviente1.walk();

    System.out.println("Humano1 " + humano1.getName());
    System.out.println("CosaViviente1 " + cosaviviente1.getName());

    boolean b1 = (humano1 == cosaviviente1);
    System.out.println("Son los mismo humano1 y  cosaviviente " + b1);

    /*
     Exa23ClaseAbstracta cosaviviente2 = new Exa23ClaseMono("Amelio");
     cosaviviente2.walk();
     */

    // Compile error
    // Exa23ClaseAbstracta z = new Exa23ClaseAbstracta();

  }
}
